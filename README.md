# My Models
A collection of models and their param file generators for different simulations.

## Installation

Using ```setyp.py```:
```bash
$ git clone https://gitlab.com/chore-assist/my_models.git
$ cd my_models
$ python3 setup.py install # --user in case you want to install it locally
```

Using ```pip```:
```bash
$ git clone https://gitlab.com/chore-assist/my_models.git
$ cd my_models
$ pip install . # --user in case you want to install it locally
$ pip3 install . # --user in case you want to install it locally
```

To install the package locally in edible mode, run:
```bash
$ pip install -e . --user
$ pip3 install -e . --user
```

## Usage

```
    >>> from my_models import <model>
    >>> m = <model>()
```


## Running tests

To execute tests, run the one of the following commands:
```bash
$ python3 ./setup.py test
```
or
```bash
$ py.test # -s to show stdout
```
from the root directory of the package.
