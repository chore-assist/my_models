from astropy.cosmology import Planck15 as cosmo
import astropy.units as u


class ClumpZoomIn:
    """Zooming-in into the cold clump / hot gas interface"""

    def __init__( M=(1e12 * u.solMass), R=(1e2 * u.kpc)):
        f_gas = cosmo.Ob0 / cosmo.Om0

        halo = {}
        halo['M'] = M
        halo['R'] = R
